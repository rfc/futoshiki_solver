"""
solving the sunday guardian's 'futoshiki' puzzle as a mip


todo:
    check for uniqueness by tacking on a
    'not the previous solution' constraint
    (e.g. sum of prev solution vars <= n solution vars - 1)
"""

from collections import namedtuple
import itertools
import pulp


# this is a simple problem.
EXAMPLE_PROBLEM_1 = """
*>*>* * *
^ ^      
* * * *>*
        v
*<* *>* *
v   v    
*>*<* * *
v   ^    
* * * *>*
"""

# this is a harder problem.
# it also starts with some cell values fixed.
EXAMPLE_PROBLEM_2 = """
*>*>*>* 5 * *
            ^
*<* * * 3 *>*
        ^    
* * * * * * *
             
7 * 4>* * * *
             
* * * * * * *
^            
* * 5>* * * 3
    v        
*<* *>* * * 5
"""


Problem = namedtuple('Problem', ['cells', 'groups', 'alphabet', 'inequalities', 'fixed'])



def make_problem(n, inequalities=None, fixed=None):
    inequalities = inequalities or []
    fixed = fixed or []
    cells = frozenset(itertools.product(range(n), repeat=2))
    groups = set()
    for i in xrange(n):
        groups.add(frozenset((i, j) for j in xrange(n)))
        groups.add(frozenset((j, i) for j in xrange(n)))
    alphabet = list(xrange(n))
    return Problem(cells, groups, alphabet, inequalities, fixed)


def load_problem(s):
    def cleaned(s):
        lines = s.split('\n')
        for line in lines:
            if line:
                yield line

    def make_inequality(i, j, c):
        if c in '<>':
            row = i/2
            col_a = (j-1)/2
            col_b = (j+2)/2
            a = (row, col_a)
            b = (row, col_b)
            if c == '>':
                a, b = b, a
            return a, b
        if c in '^v':
            row_a = (i-1)/2
            row_b = (i+1)/2
            col = j/2
            a = (row_a, col)
            b = (row_b, col)
            if c == 'v':
                a, b = b, a
            return a, b
        raise ValueError(c)

    def make_fix(i, j, c):
        if not c.isdigit():
            raise ValueError(c)
        a = int(c) - 1
        row = i/2
        col = j/2
        return ((row, col), a)

    def is_star_or_digit(c):
        return c == '*' or c.isdigit()

    ineqs = []
    fixes = []
    n = None
    for i, line in enumerate(cleaned(s)):
        if n is None:
            n = sum(is_star_or_digit(c) for c in line)

        for j, c in enumerate(line):
            if c in '<>^v':
                ineqs.append(make_inequality(i, j, c))
            elif c.isdigit():
                fixes.append(make_fix(i, j, c))
            elif c not in ' *':
                raise ValueError(c)

    return make_problem(n, ineqs, fixes)


def pose(problem):
    G = problem.cells
    H = problem.groups
    A = problem.alphabet
    I = problem.inequalities
    F = problem.fixed
    indices = list(itertools.product(G, A))


    model = pulp.LpProblem('model', pulp.LpMinimize)
    X = pulp.LpVariable.dicts('x', indices,
        lowBound=0, upBound=1, cat=pulp.LpInteger)

    # exactly one of each letter a in each group h
    for h in H:
        for a in A:
            model += sum(X[(g, a)] for g in h) == 1

    # exactly one letter in each cell
    for g in G:
        model += sum(X[(g, a)] for a in A) == 1

    for g, g_prime in I:
        assert g in G
        assert g_prime in G
        minor_terms = [float(i+1) * X[(g, a)] for (i, a) in enumerate(A)]
        major_terms = [-float(i+1) * X[(g_prime, a)] for (i, a) in enumerate(A)]
        e = sum(minor_terms + major_terms) <= -1.0 # pulp doesnt do <, only <=
        model += e

    for g, a in F:
        assert g in G
        assert a in A
        model += (X[(g, a)] == 1)

    return model, X, indices


def main():
    problem = load_problem(EXAMPLE_PROBLEM_2)
    model, X, indices = pose(problem)
    model.solve()

    answer = {}
    for i in indices:
        v = X[i].value()
        if v == 1.0:
            g, v = i
            answer[g] = v + 1

    n = len(problem.alphabet) # hack
    for i in xrange(n):
        print ' '.join(str(answer[(i, j)]) for j in xrange(n))
    print 'OKOK'

if __name__ == '__main__':
    main()

